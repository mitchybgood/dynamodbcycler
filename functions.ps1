﻿
function createDdbTable {

<#
.SYNOPSIS
    This Function creates a DynamoDB Table.
 
.DESCRIPTION
    This script takes a DynamoDB table name as an input.

.PARAMETER Tag
    Mandatory. TableName used to identity which Table to create.

.INPUTS
    As above.
 
.OUTPUTS
    N/A
 
.NOTES
  Version:        1.0
  Author:         Mitch Beaumont
  Creation Date:  27/07/2015
  Purpose/Change: Initial version.
  
.EXAMPLE
  createDdbTable -tableName = "websessions" -region ap-southeast-2
#>

    Param (
    [Parameter(Mandatory=$true)][string]$tableName,
    [Parameter(Mandatory=$true)][string]$region
    )

        Set-DefaultAWSRegion -region $region; ## Set region.
    
        $ddbSchema = New-DDBTableSchema
        $ddbSchema | Add-DDBKeySchema -KeyType "Hash" -KeyName "firstName" -KeyDataType "S"
        
        try {
            
                New-DDBTable -TableName $tableName -Schema $ddbSchema -ReadCapacity 10 -WriteCapacity 5 
            
            } catch {
                
                write-host "Error: $_" -ForegroundColor Red

            }

        try {
        
             $dims = New-Object Amazon.Cloudwatch.Model.Dimension
             $dims.Name = "TableName"
             $dims.value = $tableName
             Write-CWMetricAlarm -AlarmName "WriteCapacityLimit" -Dimensions $dims -MetricName ConsumedWriteCapacityUnits -AlarmDescription "Write Capacity Limit on Web Sessions Table" -Threshold 5 -Statistic Sum -ComparisonOperator GreaterThanThreshold -Namespace "AWS/DynamoDB" -period 300 -EvaluationPeriods 2 -region $region
             Write-CWMetricAlarm -AlarmName "ReadCapacityLimit" -Dimensions $dims -MetricName ConsumedReadCapacityUnits -AlarmDescription "Read Capacity Limit on Web Sessions Table" -Threshold 10 -Statistic Sum -ComparisonOperator GreaterThanThreshold -Namespace "AWS/DynamoDB" -period 300 -EvaluationPeriods 2 -region $region
        
        } catch {
        
             write-host "Error: $_" -ForegroundColor Red
        
        }
    }

function deleteDdbTable {

<#
.SYNOPSIS
    This Function Removes a DynamoDB Table.
 
.DESCRIPTION
    This script takes a DynamoDB table name as an input.

.PARAMETER Tag
    Mandatory. TableName used to identity which Table to remove.

.INPUTS
    As above.
 
.OUTPUTS
    N/A
 
.NOTES
  Version:        1.0
  Author:         Mitch Beaumont
  Creation Date:  27/07/2015
  Purpose/Change: Initial version.
  
.EXAMPLE
  deleteDdbTable -tableName = "websessions" -region ap-southeast-2
#>

    Param (
    [Parameter(Mandatory=$true)][string]$tableName,
    [Parameter(Mandatory=$true)][string]$region
    )

        Set-DefaultAWSRegion -region $region; ## Set region.
    
        $ddbSchema = New-DDBTableSchema
        $ddbSchema | Add-DDBKeySchema -KeyType "Hash" -KeyName "firstName" -KeyDataType "S"
        
        try {
            
                
                remove-DDBTable -TableName $tableName -Force
            
            } catch {
                
                write-host "Error: $_" -ForegroundColor Red

            }
    }

function getDdbTable {

<#
.SYNOPSIS
    This Function gets the status of a DynamoDB Table.
 
.DESCRIPTION
    This script takes a DynamoDB table name as an input.

.PARAMETER Tag
    Mandatory. TableName used to identity which Table to check the status of.

.INPUTS
    As above.
 
.OUTPUTS
    N/A
 
.NOTES
  Version:        1.0
  Author:         Mitch Beaumont
  Creation Date:  27/07/2015
  Purpose/Change: Initial version.
  
.EXAMPLE
  getDdbTable -TableName = "websessions" -region ap-southeast-2
#>

    Param (
    [Parameter(Mandatory=$true)][string]$tableName,
    [Parameter(Mandatory=$true)][string]$region
    )

        Set-DefaultAWSRegion -region $region; ## Set region.
    
        $ddbSchema = New-DDBTableSchema
        $ddbSchema | Add-DDBKeySchema -KeyType "Hash" -KeyName "firstName" -KeyDataType "S"
        
        try {
           
                if ($status = get-DDBTable -TableName $tableName | Select-Object -Property TableStatus){
                
                    return $status.value

                }else{

                    createDdbTable -TableName websessions -Region ap-southeast-2
                
                }

            } catch {
                
                write-host "Error: $_" -ForegroundColor Red

            }
    }