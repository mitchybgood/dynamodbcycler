The DynamoDB cycler script is broken down into 3 distinct files.

1. functions.ps1
2. deleteTable.ps1
3. createTable.ps1

functions.ps1:

This file includes the functions and associated DynamoDB API calls for creating, deleting and querying tables.

deleteTable.ps1:

This file dot sources the functions file and calls the deleteDdbTable table to remove the table defined in in the "-tableName" argument.

createTable.ps1:

This file dot sources the functions file and calls the createDdbTable table to create the table defined in in the "-tableName" argument.
In addition to creating the table, it will also be used to create corresponding read and write capacity alarms for the table.

Scheduling: 

Schedule the deleteTable.ps1 and createTable.ps1 scripts to run at the desired times of day.

## Comitted @ https://bitbucket.org/mitchybgood/dynamodbcycler
