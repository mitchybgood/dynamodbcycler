﻿## Schedule Task To Remove DynamoDb web sesssions table.

$ErrorActionPreference = "silentlycontinue";

Import-Module AWSPowerShell ## Import the AWS PowerShell Module

<# 

Set-AWSCredentials -AccessKey <Placeholder> -SecretKey <Placeholder>

Credentials have been removed for security reasons. It is also assumed that this script will be run from an EC2 instance with the appropriate roles.

#>


# Load Supporting Functions
. "B:\DynamoDB\functions.ps1"

deleteDdbTable -tableName  websessions -region ap-southeast-2

